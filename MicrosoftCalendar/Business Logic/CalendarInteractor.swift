//
//  CalendarInteractor.swift
//  MicrosoftCalendar
//
//  Created by Mayank on 11/11/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import Foundation

class CalendarInteractor {
    
    fileprivate func fetchCalendar() -> [Event]? {
        if let path = Bundle.main.path(forResource: "Events", ofType: "TXT") {
            do {
                let json = try Data.init(contentsOf: URL(fileURLWithPath: path))
                let eventsData = try JSONDecoder().decode([Event].self, from: json)
                if eventsData.count > 0 {
                    return eventsData
                }
            }
            catch {
                print("Error")
            }
        }
        return nil
    }
    
    fileprivate func getDateRange(event:[Event]?,onSuccess:([DateRange],[Event?])->()) {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var startAppending:Bool = false
        var startDate = dateFormatter.date(from: "2010-01-01")
        let endDate = dateFormatter.date(from: "2019-12-31")
        var dates:[DateRange] = []
        var events:[Event?] = []
        
        while startDate != endDate {
            print(startDate?.description ?? "")
            let dateRange = DateRange()
            dateRange.date = startDate
            if startAppending == false {
                let calendar = Calendar.current
                let component = calendar.dateComponents([.weekday], from: startDate!)
                if component.weekday == 1 {
                    startAppending = true
                }
            }
            if startAppending {
                let dateEvent = event?.filter({
                    return $0.date == startDate
                })
                events.append(dateEvent?.first)
                
                if dateEvent?.first != nil {
                    dateRange.isEventAvail = true
                } else {
                    dateRange.isEventAvail = false
                }
                dates.append(dateRange)
            }
            startDate = Calendar.current.date(byAdding: .day, value: 1, to: startDate!)
        }
        onSuccess(dates, events)
    }
    
    func getCurrentDateIndex(dates:[DateRange]) -> Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let current = dateFormatter.string(from: Date())
        for i in 0...dates.count-1 {
            let dString = dateFormatter.string(from: dates[i].date)
            if current == dString {
                return i
            }
        }
        return 0
    }
    
    func getEventsDataForDateRange() -> ([Event?], [DateRange])? {
        
        if let event = self.fetchCalendar() {
            self.getDateRange(event: event, onSuccess: { (date, events) in
                Globals.dateRange = date
                Globals.events = events
            })
            return (Globals.events!, Globals.dateRange!)
        }
        return nil
    }
}

//
//  MainTabBarViewController.swift
//  MicrosoftCalendar
//
//  Created by Mayank on 08/11/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import UIKit

fileprivate enum TabBarType:String {
    case mail = "Mail"
    case calendar = "Calendar"
    case file = "Files"
    case profile = "Profile"
}

class MainTabBarViewController: UITabBarController {

    fileprivate var selectedTabbar:TabBarType = .mail
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if let tabType = TabBarType(rawValue: item.title ?? "") {
            switch tabType {
            case .mail : break
            case .calendar :
                if selectedTabbar == .calendar {
                    if let vc = self.selectedViewController as? CalendarViewController {
                        vc.todayTapped()
                    }
                }
                break
            case .file : break
            case .profile : break
            }
            selectedTabbar = tabType
        }
        
    }
    

}

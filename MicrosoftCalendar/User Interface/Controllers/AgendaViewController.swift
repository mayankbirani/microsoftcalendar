//
//  AgendaViewController.swift
//  MicrosoftCalendar
//
//  Created by Mayank on 08/11/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import UIKit

class AgendaViewController: UIViewController {

    @IBOutlet weak var agendaTableView:UITableView!
    @IBOutlet weak var agendaCollectionView:UICollectionView!
    @IBOutlet weak var calendarCollectionHeight:NSLayoutConstraint!
    
    fileprivate var events: [Event?] = []
    fileprivate var dateRange:[DateRange] = []
    fileprivate let calendarInteractor = CalendarInteractor()
    fileprivate var dateCellSize:CGFloat = 0
    fileprivate var currentDateIndex:Int = 0
    fileprivate var selectDateIndex:IndexPath = IndexPath()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dateCellSize = self.agendaCollectionView.frame.size.width / 7
        
        if let event = Globals.events, let date = Globals.dateRange {
            self.events = event
            self.dateRange = date
        } else {
            if let (event, dateRange) = self.calendarInteractor.getEventsDataForDateRange() {
                self.events = event
                self.dateRange = dateRange
            }
        }
        
        currentDateIndex = self.calendarInteractor.getCurrentDateIndex(dates: self.dateRange)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.refreshToToday()
        }        
    }
    
    func refreshToToday() {
        selectDateIndex = IndexPath()
        let index = IndexPath.init(item: currentDateIndex, section: 0)
        let indexes = self.agendaCollectionView.indexPathsForVisibleItems
        if indexes.contains(index) {
            self.agendaCollectionView.reloadItems(at: indexes)
        } else {
            self.agendaCollectionView.scrollToItem(at: index, at: .centeredVertically, animated: false)
            self.agendaTableView.scrollToRow(at: IndexPath.init(row: 0, section: currentDateIndex), at: .top, animated: false)
        }
    }
}

extension AgendaViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dateRange.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell") as? EventTableViewCell {
            cell.configureCell(event: self.events[indexPath.section])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewSection") as? EventTableViewSection {
            let date = self.dateRange[section]
            cell.configureCell(date: date.date)
            return cell
        }
        return nil
    }
}
fileprivate var lastContentOffset: CGFloat = 0

extension AgendaViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == agendaCollectionView {
            if (lastContentOffset < scrollView.contentOffset.y) {
                self.calendarCollectionHeight.constant = 200
                UIView.animate(withDuration: 0.1) {
                    self.view.layoutIfNeeded()
                }
            }
            lastContentOffset = scrollView.contentOffset.y
        } else if scrollView == agendaTableView {
            self.calendarCollectionHeight.constant = 100
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

extension AgendaViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dateRange.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateCollectionViewCell", for: indexPath) as? DateCollectionViewCell {
            
            let calendar = Calendar.current
            let component = calendar.dateComponents([.month, .day], from: self.dateRange[indexPath.item].date)
            
          if component.month!%2 == 0 {
                cell.backgroundColor = UIColor.calendarGrey
            } else {
                cell.backgroundColor = UIColor.white
            }
            if indexPath.item == currentDateIndex {
                cell.dateLabel.backgroundColor = UIColor.currentDateBlue
                cell.dateLabel.textColor = UIColor.white
            } else if indexPath == selectDateIndex {
                cell.dateLabel.backgroundColor = UIColor.selectDateBlue
                cell.dateLabel.textColor = UIColor.white
            } else {
                cell.dateLabel.backgroundColor = UIColor.clear
                cell.dateLabel.textColor = UIColor.dateLabelColor
            }
            if let _ = self.events[indexPath.item] {
                cell.eventHighLightView.isHidden = false
            } else {
                cell.eventHighLightView.isHidden = true
            }
            cell.dateLabel.text = "\(component.day!)"
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: dateCellSize, height: dateCellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectDateIndex = indexPath
        collectionView.reloadItems(at: collectionView.indexPathsForVisibleItems)
        self.agendaTableView.scrollToRow(at: IndexPath.init(row: 0, section: indexPath.item) , at: .middle, animated: true)
    }
}

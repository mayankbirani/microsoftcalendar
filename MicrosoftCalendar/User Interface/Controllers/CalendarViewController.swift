//
//  CalendarViewController.swift
//  MicrosoftCalendar
//
//  Created by Mayank on 08/11/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import UIKit

fileprivate enum CalendarSelectionButtonTag:Int {
    case agenda = 101, mutliDay, dayWise
}

fileprivate let calendarTypeSelectionHeight:CGFloat = 120

class CalendarViewController: UIViewController {

    @IBOutlet weak var calendarPagingView:UIView!
    @IBOutlet weak var calendarTypeViewHeight:NSLayoutConstraint!

    fileprivate var agendaViewController:AgendaViewController?
    fileprivate var multiDayViewController:MultiDayViewController?
    fileprivate var dayViewController:DayViewController?

    fileprivate var isSelectionVisible:Bool  {
        get{
            if calendarTypeViewHeight.constant == calendarTypeSelectionHeight {
                return true
            }
            return false
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
       if let agenda = self.storyboard?.instantiateViewController(withIdentifier: "AgendaViewController") as? AgendaViewController {
            agendaViewController = agenda
            self.addChildViewController(agenda)
            agenda.view.frame = self.calendarPagingView.bounds
            self.calendarPagingView.addSubview(agenda.view)
        }
    }
    
    @IBAction func calendarTypeTapped() {
        
        if isSelectionVisible {
            self.calendarTypeViewHeight.constant = 0
            self.calendarPagingView.removeBlur()
        } else {
            self.calendarTypeViewHeight.constant = calendarTypeSelectionHeight
            self.calendarPagingView.addBlur()
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func todayTapped() {
        agendaViewController?.refreshToToday()
    }
    
    @IBAction func calendarSelectionTapped(_ sender:UIButton) {
        
        if let selectionType = CalendarSelectionButtonTag(rawValue: sender.tag) {
            self.routeCalendarSelection(selectionType: selectionType)
        }
    }
    
    fileprivate func routeCalendarSelection(selectionType:CalendarSelectionButtonTag) {
        
        self.calendarTypeTapped()
        for child in self.childViewControllers {
            child.willMove(toParentViewController: nil)
            child.view.removeFromSuperview()
            child.removeFromParentViewController()
        }
        
        switch selectionType {
            case .agenda:
            if let agenda = self.storyboard?.instantiateViewController(withIdentifier: "AgendaViewController") as? AgendaViewController {
                agendaViewController = agenda
                self.addChildViewController(agenda)
                agenda.view.frame = self.calendarPagingView.bounds
                self.calendarPagingView.addSubview(agenda.view)
            }
            break
        case .mutliDay:
                if let agenda = self.storyboard?.instantiateViewController(withIdentifier: "MultiDayViewController") as? MultiDayViewController {
                    multiDayViewController = agenda
                self.addChildViewController(agenda)
                agenda.view.frame = self.calendarPagingView.bounds
                self.calendarPagingView.addSubview(agenda.view)
            }
            break
        case .dayWise:
                if let agenda = self.storyboard?.instantiateViewController(withIdentifier: "DayViewController") as? DayViewController {
                    dayViewController = agenda
                self.addChildViewController(agenda)
                agenda.view.frame = self.calendarPagingView.bounds
                self.calendarPagingView.addSubview(agenda.view)
            }
            break
        }
    }
}

//
//  EventTableViewCell.swift
//  MicrosoftCalendar
//
//  Created by Mayank on 11/11/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    @IBOutlet weak var eventTitleLabel:UILabel!
    @IBOutlet weak var eventTypeView:UIView!
    @IBOutlet weak var eventTimeLabel:UILabel!
    @IBOutlet weak var eventVenueLabel:UILabel!
    @IBOutlet weak var eventTitleLeadingConst:NSLayoutConstraint!
    @IBOutlet weak var eventTitleNeighbourConst:NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.eventTypeView.layer.cornerRadius = self.eventTypeView.frame.size.height/2
    }
    
    func configureCell(event:Event?) {
        
        if let currentEvent = event {
            self.eventTitleLabel.text = currentEvent.eventTitle ?? "No events"
            self.eventTypeView.backgroundColor = UIColor.getEventTypeColor(eventType: currentEvent.type)
            if currentEvent.isPartial == "N" {
                self.eventTimeLabel.text = "All Day"
            } else {
                self.eventTimeLabel.text = currentEvent.from ?? "4:00"
            }
            self.eventVenueLabel.text = currentEvent.venue ?? ""
            self.eventTitleLabel.textColor = UIColor.eventTitleColor
            self.switchContraint(false)
        } else {
            self.eventTitleLabel.text = "No events"
            self.eventTimeLabel.text = ""
            self.eventTypeView.backgroundColor = nil
            self.eventVenueLabel.text = ""
            self.eventTitleLabel.textColor = UIColor.noEventColor
            self.switchContraint(true)
        }
    }
    
    fileprivate func switchContraint(_ switchConst:Bool) {
        if switchConst {
            self.eventTitleLeadingConst.priority = .defaultHigh
            self.eventTitleNeighbourConst.priority = .defaultLow
        } else {
            self.eventTitleLeadingConst.priority = .defaultLow
            self.eventTitleNeighbourConst.priority = .defaultHigh
        }
        self.layoutIfNeeded()
    }
}

class EventTableViewSection: UITableViewCell {
    
    @IBOutlet weak var eventSectionTitleLabel:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(date:Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd MMMM"
        self.eventSectionTitleLabel.text = dateFormatter.string(from: date)
    }
    
}

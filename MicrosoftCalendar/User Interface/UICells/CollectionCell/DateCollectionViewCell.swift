//
//  DateCollectionViewCell.swift
//  MicrosoftCalendar
//
//  Created by Mayank on 11/11/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import UIKit

class DateCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var eventHighLightView:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.dateLabel.layer.cornerRadius = self.dateLabel.frame.size.height/2
        self.eventHighLightView.layer.cornerRadius = self.eventHighLightView.frame.size.height/2
    }
}

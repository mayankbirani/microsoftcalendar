//
//  Event.swift
//  MicrosoftCalendar
//
//  Created by Mayank on 20/11/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import Foundation

public enum EventType:String {
    case birthDay = "birthDay"
    case anniversary = "anniversary"
    case meeting = "meeting"
    case interview = "interview"
    case date = "date"
    case reminder = "reminder"
    case other = "other"
}

struct Event : Decodable {
    
    var id: String?
    var eventTitle: String?
    private var eventDate: String?
    var date:Date? {
        get {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            if let date = dateFormatter.date(from: eventDate ?? "") {
                return date
            }
            return nil
        }
        set {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            if let date = newValue {
                eventDate = dateFormatter.string(from: date)
            }
        }
    }
    private var eventType: String?
    var type:EventType {
        get {
            if let type = EventType(rawValue: eventType ?? "") {
                return type
            }
            return .birthDay
        }
        set {
            eventType = newValue.rawValue
        }
    }
    var isPartial: String?
    var from: String?
    var to: String?
    var venue: String?
}


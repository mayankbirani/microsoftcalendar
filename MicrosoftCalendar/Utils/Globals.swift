//
//  Globals.swift
//  MicrosoftCalendar
//
//  Created by Mayank on 20/11/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import Foundation

let USER_DEFAULTS = UserDefaults.standard

struct Globals {
    static var events:[Event?]?
    static var dateRange:[DateRange]?
}

enum UserDefaultsKeys:String {
    case events = "events"
    case dateRange = "dateRange"

}

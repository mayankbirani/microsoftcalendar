//
//  UIColor.swift
//  MicrosoftCalendar
//
//  Created by Mayank on 21/11/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

/*
 
 60 236 113
 236 194 19
 
 */
import UIKit

extension UIColor {
    
    open class var calendarGrey:UIColor {
        return UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1.0)
    }
    open class var currentDateBlue:UIColor {
        return UIColor(red: 0.04, green: 0.37, blue: 0.81, alpha: 1.0)
    }
    open class var selectDateBlue:UIColor {
        return UIColor(red: 0.57, green: 0.68, blue: 0.89, alpha: 1.0)
    }
    open class var dateLabelColor:UIColor {
        return UIColor(red: 0.52, green: 0.52, blue: 0.52, alpha: 1.0)
    }
    open class var eventTitleColor:UIColor {
        return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1.0)
    }
    open class var noEventColor:UIColor {
        return UIColor(red: 0.65, green: 0.65, blue: 0.65, alpha: 1.0)
    }
    
    open class func getEventTypeColor(eventType:EventType) -> UIColor {
        
        switch eventType {
        case .birthDay: return UIColor(red: 0.45, green: 0.64, blue: 0.92, alpha: 1.0)
        case .anniversary : return UIColor(red: 0.92, green: 0.53, blue: 0.69, alpha: 1.0)
        case .meeting : return UIColor(red: 0.49, green: 0.10, blue: 0.92, alpha: 1.0)
        case .interview : return UIColor(red: 0.23, green: 0.92, blue: 0.44, alpha: 1.0)
        case .date : return UIColor(red: 0.92, green: 0.76, blue: 0.07, alpha: 1.0)
        case .reminder : return UIColor(red: 0.49, green: 0.10, blue: 0.92, alpha: 1.0)
        case .other : return UIColor(red: 0.49, green: 0.10, blue: 0.92, alpha: 1.0)
        }
    }
}

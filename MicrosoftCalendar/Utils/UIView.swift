//
//  UIView.swift
//  MicrosoftCalendar
//
//  Created by Mayank on 11/11/17.
//  Copyright © 2017 Mayank. All rights reserved.
//

import UIKit

extension UIView {
    
    func addBlur() {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
    
    func removeBlur() {
        for blur in self.subviews {
            if let b = blur as? UIVisualEffectView {
                b.removeFromSuperview()
                break
            }
        }
    }
}
